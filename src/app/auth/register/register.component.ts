import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthResponseData, AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  error: string = null;
  Roles: any = ['Admin', 'Author', 'Reader'];

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.registerForm = new FormGroup({
      'userName': new FormControl(''),
      'email': new FormControl(''),
      'password': new FormControl('')
    });
  }

  onSignUp() {

    if (!this.registerForm.valid) {
      return;
    }
    const email = this.registerForm.value.email;
    const password = this.registerForm.value.password;

    let authObs: Observable<AuthResponseData>;
    authObs = this.authService.signup(email, password);
    authObs.subscribe(
      resData => {
        console.log(resData);
        this.router.navigate(['/home']);
      },
      errorMessage => {
        console.log(errorMessage);
        this.error = errorMessage;
      }
    );

    this.registerForm.reset();
  }
}