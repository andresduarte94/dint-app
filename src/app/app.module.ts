import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HomeComponent } from './home/home.component';
import { CustodyDashboardComponent } from './home/custody-dashboard/custody-dashboard.component';
import { DialogContentExampleDialog } from './shared/transaction-dialog/transaction-dialog.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ProjectsDashboardComponent } from './home/projects-dashboard/projects-dashboard.component';
import { InvestorsDashboardComponent } from './home/investors-dashboard/investors-dashboard.component';
import { ProjectInvestorComponent } from './project-investor/project-investor.component';
import { ProjectBuilderComponent } from './project-builder/project-builder.component';
import { AngularMaterialModule } from './shared/angular-material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { MyInvestmentListComponent } from './my-investment-list/my-investment-list.component';
import { MyProjectListComponent } from './my-project-list/my-project-list.component';

@NgModule({
  declarations: [
    AppComponent,
    DialogContentExampleDialog,
    HomeComponent,
    ProjectInvestorComponent,
    ProjectBuilderComponent,
    CustodyDashboardComponent,
    ProjectsDashboardComponent,
    InvestorsDashboardComponent,
    RegisterComponent,
    LoginComponent,
    MyInvestmentListComponent,
    MyProjectListComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    FlexLayoutModule
  ],
  entryComponents: [DialogContentExampleDialog],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }