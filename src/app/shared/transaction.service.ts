import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { ProjectState } from './project-state.model';
import { RequestService } from './request.service';
import { DialogContentExampleDialog } from './transaction-dialog/transaction-dialog.component';

@Injectable({ providedIn: 'root' })
export class TransactionService {
    private _transactionDetails: string = '';
    private _projectStateInfo: any = {};
    public projectStateInfoUpdate: Subject<ProjectState> = new Subject();

    constructor(public dialog: MatDialog, private requestService: RequestService) {}

    public get transactionDetails(): string {
        return this._transactionDetails;
    }

    public set transactionDetails(value: string) {
        this._transactionDetails = value;
    }

    public get projectStateInfo(): any {
        return this._projectStateInfo;
    }

    public set projectStateInfo(value: any) {
        this._projectStateInfo = value;
    }

    openTransacionResultModal() {
        const dialogRef = this.dialog.open(DialogContentExampleDialog, {
            width: '75%',
            data: { 'transactionDetails': this.transactionDetails }
        });

        dialogRef.afterClosed().subscribe(result => {
            this.requestService.getRequest('http://localhost:10009/api/iou/project-state')
            .subscribe(response => {
              const entireData = response;
              this.projectStateInfo.totalPaid = entireData[entireData.length - 1].state.data.totalPaid
              this.projectStateInfo.lastPayment = entireData[entireData.length - 1].state.data.paid
              this.projectStateInfo.workCompleted = entireData[entireData.length - 1].state.data.workCompletion
              this.projectStateInfo.allocatedAmount = entireData[entireData.length - 1].state.data.allocatedAmount
              // Update project state info in service
              this.projectStateInfoUpdate.next(this.projectStateInfo);
            });
        });
    }

}