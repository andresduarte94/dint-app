import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class RequestService {
    private headerDictGet = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Headers': 'X-Requested-With,Content-Type',
        'Access-Control-Allow-Origin': 'http://localhost:4200',
        'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS'
    }
    private headerDictPut = {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS'
    }

    constructor(private http: HttpClient) { }

    getRequest(url: string) {
        return this.http.get<any>(url, {
            headers: new HttpHeaders(this.headerDictGet),
            responseType: 'json'
        });
    }

    putRequest(url: string, queryString: string = '') {
        const fullUrl = url + '?' + queryString;
        return this.http.put(fullUrl, {}, {
            headers: new HttpHeaders(this.headerDictPut),
            responseType: 'text'
        });
    }
}