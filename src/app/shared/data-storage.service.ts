import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';

import { DatabaseService } from './database.service';

@Injectable({ providedIn: 'root' })
export class DataStorageService {
  constructor(
    private http: HttpClient,
    private databaseService: DatabaseService,
  ) { }

  fetchUsers() {
    return this.http
      .get<any[]>(
        'https://expanded-port-253513.firebaseio.com/users.json'
      )
      .pipe(
        map(usersJson => {
          let users = [];
          for (let [i, [fbId, user]] of Object.entries(Object.entries(usersJson))) {
            users[i] = user;
          };
          return users;
        }),
        tap(users => {
          this.databaseService.users = users;
        })
      );
  }

  fetchProjects() {
    return this.http
      .get<any[]>(
        'https://expanded-port-253513.firebaseio.com/projects.json'
      )
      .pipe(
        map(projectsJson => {
          let projects = [];
          for (let [i, [fbId, project]] of Object.entries(Object.entries(projectsJson))) {
              var formatter = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: project.currency,
              });
              project.investment = formatter.format(project.totalInvestment);
              projects[i] = project;
          };
          return projects;
        }),
        tap(projects => {
          this.databaseService.projects = projects;
        })
      );
  }
}