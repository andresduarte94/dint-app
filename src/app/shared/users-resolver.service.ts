import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';

import { DataStorageService } from '../shared/data-storage.service';
import { DatabaseService } from './database.service';

@Injectable({ providedIn: 'root' })
export class UsersResolverService implements Resolve<any[]> {
  constructor(
    private dataStorageService: DataStorageService,
    private databaseService: DatabaseService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const users = this.databaseService.users;
    if (users.length === 0) {
      return this.dataStorageService.fetchUsers();
    } else {
      return users;
    }
  }
}