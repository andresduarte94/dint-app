export class User {

    constructor(
        public name: string,
        public userId: string,
        public email: string,
        public workField: string,
        public country: string,
        public userType: string
    ) { }
}