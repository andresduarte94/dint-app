import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'transaction-dialog',
  templateUrl: 'transaction-dialog.html',
})
export class DialogContentExampleDialog {
  transactionIdInfo: string = '';

  constructor(
    public dialogRef: MatDialogRef<DialogContentExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

    ngOnInit() {
      const transactionDetails = this.data.transactionDetails;
      this.transactionIdInfo = transactionDetails.substr(0, transactionDetails.indexOf("ledger") + 6);
    }
}