export class Project {

    constructor(
        public projectId: string,
        public name: string,
        public description: string,
        public totalInvestment: number,
        public currency: string,
        public initialPercentage: number,
        public builderId: string,
        public numberOfTasks: number,
        public investment: string,
    ) { }
}