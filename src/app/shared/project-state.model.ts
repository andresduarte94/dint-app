export class ProjectState {

    constructor(
        public totalPaid: string,
        public lastPayment: string,
        public workCompleted: number,
        public allocatedAmount: string,
    ) { }
}