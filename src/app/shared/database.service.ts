import { Injectable } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Project } from './project.model';
import { User } from './user.model';

@Injectable({ providedIn: 'root' })
export class DatabaseService {
    private _users: User[] = [];
    private _projects: Project[] = [];

    constructor(private authService: AuthService) { }

    public get users(): User[] {
        return this._users;
    }
    public set users(value: User[]) {
        this._users = value;
    }

    public get projects(): Project[] {
        return this._projects;
    }
    public set projects(value: Project[]) {
        this._projects = value;
    }

    getUserById(id: string): User {
        const user = this.users.filter((user: User) => {
            return (user.userId === id); 
        })[0];
        return user;
    }
 
    getProjectById(id: string): Project {
        const project = this.projects.filter((project: Project) => { 
            return (project.projectId == id); 
        })[0];
        return project;
    }

    getMyProjects(): Project[] {
        const userId = JSON.parse(localStorage.getItem('userData')).id;
        const projects = this.projects.filter((project: Project) => { 
            return (project.builderId == userId); 
        });
        return projects;
    }
}