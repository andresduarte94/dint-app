import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { HomeComponent } from './home/home.component';
import { MyInvestmentListComponent } from './my-investment-list/my-investment-list.component';
import { MyProjectListComponent } from './my-project-list/my-project-list.component';
import { ProjectBuilderComponent } from './project-builder/project-builder.component';
import { ProjectInvestorComponent } from './project-investor/project-investor.component';
import { ProjectsResolverService } from './shared/projects-resolver.service';
import { UsersResolverService } from './shared/users-resolver.service';

const appRoutes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    resolve: [ProjectsResolverService]
  },
  {
    path: 'project',
    canActivate: [AuthGuard],
    resolve: [ProjectsResolverService, UsersResolverService],
    children: [
      {
        path: 'investor/:projectId',
        component: ProjectInvestorComponent,
      },
      {
        path: 'builder/:projectId',
        component: ProjectBuilderComponent,
      }
    ]
  },
  {
    path: 'me',
    canActivate: [AuthGuard],
    resolve: [ProjectsResolverService],
    children: [
      {
        path: 'projects',
        component: MyProjectListComponent,
      },
      {
        path: 'investments',
        component: MyInvestmentListComponent,
      }
    ]
  },
  {
    path: 'auth',
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'register', component: RegisterComponent },
    ]
  },
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, { scrollPositionRestoration: 'top', preloadingStrategy: PreloadAllModules }) //,, enableTracing: true 
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }