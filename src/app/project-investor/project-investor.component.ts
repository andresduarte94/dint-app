import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DatabaseService } from 'src/app/shared/database.service';
import { ProjectState } from 'src/app/shared/project-state.model';
import { Project } from 'src/app/shared/project.model';
import { RequestService } from 'src/app/shared/request.service';
import { TransactionService } from 'src/app/shared/transaction.service';
import { User } from '../shared/user.model';

@Component({
  selector: 'app-investor-dashboard',
  templateUrl: './project-investor.component.html',
  styleUrls: ['./project-investor.component.scss']
})
export class ProjectInvestorComponent implements OnInit {
  me: string = '';
  project: Project;
  projectState: ProjectState;
  currencies: string[] = ['USD', 'GBP', 'EUR', 'JPY', 'RUB'];
  transactionDetails: string;
  getProfile: Subscription;
  getProjectState: Subscription;
  allocateTransacion: Subscription;
  resetTransaction: Subscription;
  updateProjectState: Subscription;
  investorForm: FormGroup;
  builder: User;

  constructor(private activatedRoute: ActivatedRoute, public requestService: RequestService, private transactionService: TransactionService, private databaseService: DatabaseService) { }

  ngOnInit(): void {
    this.projectState = this.transactionService.projectStateInfo;
    this.project = this.databaseService.getProjectById(this.activatedRoute.snapshot.params.projectId);
    this.builder = this.databaseService.getUserById(this.project.builderId)

    /*     this.getProfile = this.requestService.getRequest('http://localhost:10009/api/iou/me')
          .subscribe(response => {
            this.me = response.me;
          }, (error) => {
            console.log(error);
          }); */
    /*     this.getProjectState = this.requestService.getRequest('http://localhost:10009/api/iou/project-state')
          .subscribe(response => {
            const entireData = response;
            this.projectState.totalPaid = entireData[entireData.length - 1].state.data.totalPaid
            this.projectState.lastPayment = entireData[entireData.length - 1].state.data.paid
            this.projectState.workCompleted = entireData[entireData.length - 1].state.data.workCompletion
            this.projectState.allocatedAmount = entireData[entireData.length - 1].state.data.allocatedAmount
            // Update project state info in service
            this.transactionService.projectStateInfo = this.projectState;
          }, (error) => {
            console.log(error);
          }); */

    this.updateProjectState = this.transactionService.projectStateInfoUpdate.subscribe((projectStateInfo) => {
      this.projectState = projectStateInfo
    });

    //Create and initialize investor form
    this.investorForm = new FormGroup({
      'amountToAllocate': new FormControl(this.project.totalInvestment),
      'currency': new FormControl(this.project.currency),
      'totalPayments': new FormControl(this.project.numberOfTasks),
      'initialPercent': new FormControl(this.project.initialPercentage),
    });
  }

  allocateTransaction() {
    const amount = this.investorForm.controls['amountToAllocate'].value;
    const currency = this.investorForm.controls['currency'].value;
    const totalPayments = this.investorForm.controls['totalPayments'].value;
    const initialPercent = this.investorForm.controls['initialPercent'].value;

    const params = {
      amount: amount,
      currency: currency,
      party: "O=CustodyService,L=Paris,C=FR",
      totalPayments: totalPayments,
      initialPercent: initialPercent
    };
    const queryString = new URLSearchParams(params).toString();

    this.allocateTransacion = this.requestService.putRequest('http://localhost:10009/api/iou/allocate-project', queryString)
      .subscribe(response => {
        this.transactionService.transactionDetails = response;
        this.transactionService.openTransacionResultModal();
      });
  }

  reset() {
    const currency = this.investorForm.controls['currency'].value;

    const params = {
      currency: currency,
    };
    const queryString = new URLSearchParams(params).toString();

    this.resetTransaction = this.requestService.putRequest('http://localhost:10009/api/iou/reset', queryString)
      .subscribe(response => {
        const entireData = JSON.parse(response);
        this.projectState.totalPaid = entireData[entireData.length - 1].state.data.totalPaid
        this.projectState.lastPayment = entireData[entireData.length - 1].state.data.paid
        this.projectState.workCompleted = entireData[entireData.length - 1].state.data.workCompletion
        this.projectState.allocatedAmount = entireData[entireData.length - 1].state.data.allocatedAmount
        // Update project state info in service
        this.transactionService.projectStateInfo = this.projectState;
      });
  }

  ngOnDestroy() {
    if (this.getProfile) {
      this.getProfile.unsubscribe();
    }
    if (this.getProjectState) {
      this.getProjectState.unsubscribe();
    }
    if (this.allocateTransacion) {
      this.allocateTransacion.unsubscribe();
    }
    if (this.resetTransaction) {
      this.resetTransaction.unsubscribe();
    }
    if (this.updateProjectState) {
      this.updateProjectState.unsubscribe();
    }
  }
}
