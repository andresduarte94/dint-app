import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { DatabaseService } from '../shared/database.service';
import { Project } from '../shared/project.model';

@Component({
  selector: 'app-my-project-list',
  templateUrl: './my-project-list.component.html',
  styleUrls: ['./my-project-list.component.scss']
})
export class MyProjectListComponent implements OnInit, AfterViewInit {
  projects: Project[] = [];
  displayedColumns: string[] = ['name', 'description', 'investment', 'initialPercentage', 'actions'];
  dataSource = new MatTableDataSource<Project>(null);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private databaseService: DatabaseService) { }

  ngOnInit(): void {
    this.projects = this.databaseService.getMyProjects();
    this.dataSource = new MatTableDataSource<Project>(this.projects);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
}