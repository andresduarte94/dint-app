import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ProjectState } from 'src/app/shared/project-state.model';
import { RequestService } from 'src/app/shared/request.service';
import { TransactionService } from 'src/app/shared/transaction.service';

@Component({
  selector: 'app-custody-dashboard',
  templateUrl: './custody-dashboard.component.html',
  styleUrls: ['./custody-dashboard.component.scss']
})
export class CustodyDashboardComponent implements OnInit {
  me: string = '';
  projectState: ProjectState;
  transactionDetails: string;
  getProfile: Subscription;
  getProjectState: Subscription;
  resetTransaction: Subscription;
  updateProjectState: Subscription;

  constructor(public requestService: RequestService, private transactionService: TransactionService) { }

  ngOnInit(): void {
    this.projectState = this.transactionService.projectStateInfo;
    this.getProfile = this.requestService.getRequest('http://localhost:10015/api/iou/me')
    .subscribe(response => {
      this.me = response.me;
    });

    this.getProjectState = this.requestService.getRequest('http://localhost:10009/api/iou/project-state')
    .subscribe(response => {
      this.transactionService.projectStateInfo = {};
      const entireData = response;
      this.projectState.totalPaid = entireData[entireData.length - 1].state.data.totalPaid
      this.projectState.lastPayment = entireData[entireData.length - 1].state.data.paid
      this.projectState.workCompleted = entireData[entireData.length - 1].state.data.workCompletion
      this.projectState.allocatedAmount = entireData[entireData.length - 1].state.data.allocatedAmount
      // Update project state info in service
      this.transactionService.projectStateInfo = this.projectState;
    });

    this.updateProjectState = this.transactionService.projectStateInfoUpdate.subscribe((projectStateInfo) => {
      this.projectState = projectStateInfo
    })
  }

   reset() {
    this.resetTransaction = this.requestService.putRequest('http://localhost:10009/api/iou/reset')
      .subscribe(response => {
        this.transactionService.projectStateInfo = {};
        const entireData = JSON.parse(response);
        this.projectState.totalPaid = entireData[entireData.length - 1].state.data.totalPaid
        this.projectState.lastPayment = entireData[entireData.length - 1].state.data.paid
        this.projectState.workCompleted = entireData[entireData.length - 1].state.data.workCompletion
        this.projectState.allocatedAmount = entireData[entireData.length - 1].state.data.allocatedAmount
        // Update project state info in service
        this.transactionService.projectStateInfo = this.projectState;
      });
  } 

  ngOnDestroy() {
    if (this.getProfile) {
      this.getProfile.unsubscribe();
    }
    if (this.getProjectState) {
      this.getProjectState.unsubscribe();
    }
    if (this.resetTransaction) {
      this.resetTransaction.unsubscribe();
    }
    if (this.updateProjectState) {
      this.updateProjectState.unsubscribe();
    }
  }
}
