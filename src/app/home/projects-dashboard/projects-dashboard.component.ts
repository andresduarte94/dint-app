import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { DatabaseService } from 'src/app/shared/database.service';
import { Project } from 'src/app/shared/project.model';

@Component({
  selector: 'app-projects-dashboard',
  templateUrl: './projects-dashboard.component.html',
  styleUrls: ['./projects-dashboard.component.scss']
})
export class ProjectsDashboardComponent implements OnInit, AfterViewInit {
  projects: Project[] = [];
  displayedColumns: string[] = ['name', 'description', 'investment', 'initialPercentage', 'actions'];
  dataSource = new MatTableDataSource<Project>(null);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private databaseService: DatabaseService) { }

  ngOnInit(): void {

    this.projects = this.databaseService.projects;
    this.dataSource = new MatTableDataSource<Project>(this.projects);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
}