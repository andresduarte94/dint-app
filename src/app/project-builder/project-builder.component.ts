import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DatabaseService } from 'src/app/shared/database.service';
import { ProjectState } from 'src/app/shared/project-state.model';
import { Project } from 'src/app/shared/project.model';
import { RequestService } from 'src/app/shared/request.service';
import { TransactionService } from 'src/app/shared/transaction.service';
import { User } from '../shared/user.model';

@Component({
  selector: 'app-builder-dashboard',
  templateUrl: './project-builder.component.html',
  styleUrls: ['./project-builder.component.scss']
})
export class ProjectBuilderComponent implements OnInit {
  me: string = '';
  project: Project;
  projectState: ProjectState;
  transactionDetails: string;
  getProfile: Subscription;
  getProjectState: Subscription;
  fullTransaction: Subscription;
  halfTransaction: Subscription;
  resetTransaction: Subscription;
  updateProjectState: Subscription;
  builder: User;

  constructor(private activatedRoute: ActivatedRoute, public requestService: RequestService, private transactionService: TransactionService, private databaseService: DatabaseService) { }

  ngOnInit(): void {
    this.projectState = this.transactionService.projectStateInfo;
    this.project = this.databaseService.getProjectById(this.activatedRoute.snapshot.params.projectId);
    this.builder = this.databaseService.getUserById(this.project.builderId);

    /*     this.getProfile = this.requestService.getRequest('http://localhost:10009/api/iou/me')
          .subscribe(response => {
            this.me = response.me;
          }, (error) => {
            console.log(error);
          }); */
    /*     this.getProjectState = this.requestService.getRequest('http://localhost:10009/api/iou/project-state')
          .subscribe(response => {
            const entireData = response;
            this.projectState.totalPaid = entireData[entireData.length - 1].state.data.totalPaid
            this.projectState.lastPayment = entireData[entireData.length - 1].state.data.paid
            this.projectState.workCompleted = entireData[entireData.length - 1].state.data.workCompletion
            this.projectState.allocatedAmount = entireData[entireData.length - 1].state.data.allocatedAmount
            // Update project state info in service
            this.transactionService.projectStateInfo = this.projectState;
          }, (error) => {
            console.log(error);
          }); */

    this.updateProjectState = this.transactionService.projectStateInfoUpdate.subscribe((projectStateInfo) => {
      this.projectState = projectStateInfo
    });
  }

  unlockNextPayment() {
    const params = {
      currency: this.projectState.totalPaid.slice(-3),
      party: "O=Builder,L=New York,C=US"
    };
    const queryString = new URLSearchParams(params).toString();
    this.halfTransaction = this.requestService.putRequest('http://localhost:10009/api/iou/complete-work', queryString)
      .subscribe(response => {
        this.transactionService.transactionDetails = response;
        this.transactionService.openTransacionResultModal();
      });
  }

  reset() {
    this.resetTransaction = this.requestService.putRequest('http://localhost:10009/api/iou/reset')
      .subscribe(response => {
        this.transactionService.projectStateInfo = {};
        const entireData = JSON.parse(response);
        this.projectState.totalPaid = entireData[entireData.length - 1].state.data.totalPaid
        this.projectState.lastPayment = entireData[entireData.length - 1].state.data.paid
        this.projectState.workCompleted = entireData[entireData.length - 1].state.data.workCompletion
        this.projectState.allocatedAmount = entireData[entireData.length - 1].state.data.allocatedAmount
        // Update project state info in service
        this.transactionService.projectStateInfo = this.projectState;
      });
  }

  ngOnDestroy() {
    if (this.getProfile) {
      this.getProfile.unsubscribe();
    }
    if (this.getProjectState) {
      this.getProjectState.unsubscribe();
    }
    if (this.fullTransaction) {
      this.fullTransaction.unsubscribe();
    }
    if (this.halfTransaction) {
      this.halfTransaction.unsubscribe();
    }
    if (this.resetTransaction) {
      this.resetTransaction.unsubscribe();
    }
    if (this.updateProjectState) {
      this.updateProjectState.unsubscribe();
    }
  }
}
